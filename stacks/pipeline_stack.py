from aws_cdk import (
    core,
    aws_codepipeline as codepipeline,
    aws_codepipeline_actions as codepipeline_actions,
    aws_codebuild as codebuild,
    aws_iam as iam,
    aws_s3 as s3
)

class PipelineStack(core.Stack):

    def __init__(self, app: core.App, id: str, props, **kwargs) -> None:
        super().__init__(app, id, **kwargs)

        self.namespace = app.node.try_get_context("namespace")
        self.codestar_connection_arn = app.node.try_get_context("codestar-connection-arn")
        self.codepipeline_aws_account = app.node.try_get_context("codepipeline_aws_account")
        self.khelo_web_test_bucket = s3.Bucket(self, "KheloWebTestBucket",
            bucket_name="test-khelo-web",
            block_public_access=s3.BlockPublicAccess(block_public_policy=False, block_public_acls=False),
            public_read_access=True,
            website_index_document="index.html"
        )
        
        self.pipeline_artifacts_bucket = s3.Bucket(self, "PipelineArtifactsBucket",
            bucket_name=f"{self.namespace.lower()}-pipeline-artifacts-bucket"
        )

        source_artifacts = {
            "khelo-cdk": codepipeline.Artifact(artifact_name="khelo-cdk"),
            "khelo-hybrid": codepipeline.Artifact(artifact_name="khelo-hybrid"),
            "khelo-server": codepipeline.Artifact(artifact_name="khelo-server"),
            "khelo-web": codepipeline.Artifact(artifact_name="khelo-web")
        }

        build_outputs = {
            "khelo-cdk": codepipeline.Artifact(artifact_name="khelo-cdk-build-output"),
            "khelo-server": codepipeline.Artifact(artifact_name="khelo-server-build-output"),
            "khelo-hybrid": codepipeline.Artifact(artifact_name="khelo-hybrid-build-output"),
            "khelo-web": codepipeline.Artifact(artifact_name="khelo-web-build-output")
        }

        self.bitbucket_source_action_role = iam.Role(self, "BitBucketSourceActionRole",
            role_name="BitBucketSourceActionRole",
            assumed_by=iam.AccountPrincipal(self.codepipeline_aws_account),
            inline_policies={
                "BitBucketSourceActionRoleInlinePolicy": iam.PolicyDocument(
                    statements=[
                        iam.PolicyStatement(
                            effect=iam.Effect.ALLOW,
                            actions=[
                                "codestar-connections:UseConnection"
                            ],
                            resources=[
                                self.codestar_connection_arn
                            ]
                        ),
                        iam.PolicyStatement(
                            actions=[
                                "s3:GetObject*",
                                "s3:GetBucket*",
                                "s3:List*",
                                "s3:DeleteObject*",
                                "s3:PutObject",
                                "s3:Abort*",
                                "s3:PutObjectAcl"
                            ],
                            effect=iam.Effect.ALLOW,
                            resources=[
                                f"{self.pipeline_artifacts_bucket.bucket_arn}",
                                f"{self.pipeline_artifacts_bucket.bucket_arn}/*"
                            ]
                        )
                    ]
                )
            }
        )

        khelo_cdk_repo = codepipeline_actions.BitBucketSourceAction(
            connection_arn=self.codestar_connection_arn,
            repo="khelo-cdk", # Needs to be lowercase even if uppercase in BitBucket 
            branch="main",
            output=source_artifacts["khelo-cdk"],
            owner="khelo-app", # Corresponds to the BitBucket workspace name, must be same
            action_name="KheloCDKSource",
            role=self.bitbucket_source_action_role
        )

        khelo_server_repo = codepipeline_actions.BitBucketSourceAction(
            connection_arn=self.codestar_connection_arn,
            repo="khelo-server", # Needs eto be lowercase even if uppercase in BitBucket 
            branch="main",
            output=source_artifacts["khelo-server"],
            owner="khelo-app", # Corresponds to the BitBucket workspace name, must be same
            action_name="KheloServerSource",
            role=self.bitbucket_source_action_role
        )

        khelo_hybrid_repo = codepipeline_actions.BitBucketSourceAction(
            connection_arn=self.codestar_connection_arn,
            repo="khelo-hybrid", # Needs to be lowercase even if uppercase in BitBucket 
            branch="main",
            output=source_artifacts["khelo-hybrid"],
            owner="khelo-app", # Corresponds to the BitBucket workspace name, must be same
            action_name="KheloHybridSource",
            role=self.bitbucket_source_action_role
        )

        khelo_web_repo = codepipeline_actions.BitBucketSourceAction(
            connection_arn=self.codestar_connection_arn,
            repo="khelo-web", # Needs to be lowercase even if uppercase in BitBucket 
            branch="main",
            output=source_artifacts["khelo-web"],
            owner="khelo-app", # Corresponds to the BitBucket workspace name, must be same
            action_name="KheloWebSource",
            role=self.bitbucket_source_action_role
        )

        khelo_cdk_build_project_name = f"{self.namespace.capitalize()}CDKBuildProject"
        khelo_cdk_build = codebuild.PipelineProject(self, khelo_cdk_build_project_name,
            project_name=khelo_cdk_build_project_name,
            environment=dict(
                build_image=codebuild.LinuxBuildImage.STANDARD_5_0
            )
        )

        khelo_server_build_project_name = f"{self.namespace.capitalize()}ServerBuildProject"
        khelo_server_build = codebuild.Project(self, khelo_server_build_project_name,
            project_name=khelo_server_build_project_name,
            build_spec=codebuild.BuildSpec.from_object({
                "version": "0.2",
                "phases": {
                    "build": {
                        "commands": ["echo \"Hello, CodeBuild!\""
                        ]
                    }
                }
            })
        )

        khelo_hybrid_build_project_name = f"{self.namespace.capitalize()}HybridBuildProject"
        khelo_hybrid_build = codebuild.Project(self, khelo_hybrid_build_project_name,
            project_name=khelo_hybrid_build_project_name,
            build_spec=codebuild.BuildSpec.from_object({
                "version": "0.2",
                "phases": {
                    "build": {
                        "commands": ["echo \"Hello, CodeBuild!\""
                        ]
                    }
                }
            })
        )

        khelo_web_build_project_name = f"{self.namespace.capitalize()}WebBuildProject"
        khelo_web_build = codebuild.PipelineProject(self, khelo_web_build_project_name,
            project_name=khelo_web_build_project_name,
            environment=dict(
                build_image=codebuild.LinuxBuildImage.STANDARD_5_0
            )
        )

        khelo_code_build_role_name = f"{self.namespace.capitalize()}CodeBuildRole"
        self.codebuild_role = iam.Role(self, khelo_code_build_role_name,
            role_name=khelo_code_build_role_name,
            assumed_by=iam.AccountPrincipal(self.codepipeline_aws_account),
            inline_policies={
                f"{self.namespace.capitalize()}CodeBuildRoleInlinePolicy": iam.PolicyDocument(
                    statements=[
                        iam.PolicyStatement(
                            actions=[
                                "codebuild:BatchGetBuilds",
                                "codebuild:StartBuild",
                                "codebuild:StopBuild"
                            ],
                            resources=[
                                f"{khelo_cdk_build.project_arn}",
                                f"{khelo_server_build.project_arn}",
                                f"{khelo_hybrid_build.project_arn}",
                                f"{khelo_web_build.project_arn}"
                            ],
                            effect=iam.Effect.ALLOW
                        )
                    ]
                )
            }
        )

        khelo_pipeline_role_name = f"{self.namespace.capitalize()}PipelineRole"
        self.pipeline_role = iam.Role(self, khelo_pipeline_role_name,
            role_name=khelo_pipeline_role_name,
            assumed_by=iam.ServicePrincipal("codepipeline.amazonaws.com"),
            inline_policies={
                f"{self.namespace.capitalize()}PipelineRoleInlinePolicy": iam.PolicyDocument(
                    statements=[
                        iam.PolicyStatement(
                            actions=[
                                "sts:AssumeRole"
                            ],
                            resources=[
                                f"{self.bitbucket_source_action_role.role_arn}",
                                f"{self.codebuild_role.role_arn}"
                            ],
                            effect=iam.Effect.ALLOW
                        )
                    ]
                )
            }
        )

        khelo_pipeline_name = f"{self.namespace.capitalize()}Pipeline"
        pipeline = codepipeline.Pipeline(self, khelo_pipeline_name,
            role=self.pipeline_role,
            artifact_bucket=self.pipeline_artifacts_bucket,
            stages=[
                codepipeline.StageProps(stage_name="Source", 
                    actions= [
                        khelo_cdk_repo, 
                        khelo_server_repo,
                        khelo_hybrid_repo,
                        khelo_web_repo
                    ]
                ),
                codepipeline.StageProps(stage_name="Build", 
                    actions=[
                        codepipeline_actions.CodeBuildAction(
                            action_name="khelo-cdk-build",
                            project=khelo_cdk_build,
                            input=source_artifacts["khelo-cdk"],
                            outputs=[build_outputs["khelo-cdk"]],
                            role=self.codebuild_role
                        ),
                        codepipeline_actions.CodeBuildAction(
                            action_name="khelo-server-build",
                            project=khelo_server_build,
                            input=source_artifacts["khelo-server"],
                            outputs=[build_outputs["khelo-server"]],
                            role=self.codebuild_role
                        ),
                        codepipeline_actions.CodeBuildAction(
                            action_name="khelo-hybrid-build",
                            project=khelo_hybrid_build,
                            input=source_artifacts["khelo-hybrid"],
                            outputs=[build_outputs["khelo-hybrid"]],
                            role=self.codebuild_role
                        ),
                        codepipeline_actions.CodeBuildAction(
                            action_name="khelo-web-build",
                            project=khelo_web_build,
                            input=source_artifacts["khelo-web"],
                            outputs=[build_outputs["khelo-web"]],
                            role=self.codebuild_role
                        )
                    ]
                ),
                codepipeline.StageProps(stage_name="Test",
                    actions=[
                        codepipeline_actions.S3DeployAction(
                            action_name="test-khelo-web-deployment",
                            bucket=self.khelo_web_test_bucket,
                            input=build_outputs["khelo-web"],
                            extract=True
                        )
                    ]
                )
            ]
        )
