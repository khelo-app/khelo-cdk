from aws_cdk import (
    core,
    aws_iam as iam,
    aws_cognito as cognito
)

class AuthStack(core.Stack):

    def __init__(self, app: core.App, id: str, props, **kwargs) -> None:
        super().__init__(app, id, **kwargs)

        user_pool = cognito.UserPool(self, "KheloUserPool",
            self_sign_up_enabled=True,
            auto_verify=cognito.AutoVerifiedAttrs(email=True)
        )

        user_pool_client = cognito.UserPoolClient(self, "KheloUserPoolClient",
            user_pool=user_pool,
            generate_secret=False
        )

        identity_pool = cognito.CfnIdentityPool(self, "KheloIdentityPool",
            allow_unauthenticated_identities=True,
            cognito_identity_providers=[
                cognito.CfnIdentityPool.CognitoIdentityProviderProperty(
                    client_id=user_pool_client.user_pool_client_id,
                    provider_name=user_pool.user_pool_provider_name
                )
            ]
        )

        authenticated_role = iam.Role(self, "KheloDefaultAuthenticatedRole",
            assumed_by=iam.FederatedPrincipal(
                federated="cognito-identity.amazonaws.com",
                conditions={
                    "StringEquals": {
                        "cognito-identity.amazonaws.com:aud": identity_pool.ref
                    },
                    "ForAnyValue:StringLike": {
                        "cognito-identity.amazonaws.com:amr": "authenticated"
                    }
                },
                assume_role_action="sts:AssumeRoleWithWebIdentity"
            )
        )

        authenticated_role.add_to_policy(iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=[
                "cognito-sync:*",
                "cognito-identity:*"
            ],
            resources=["*"]
        ))

        unauthenticated_role = iam.Role(self, "KheloDefaultUnauthenticatedRole", 
            assumed_by=iam.FederatedPrincipal(
                federated="cognito-identity.amazonaws.com",
                conditions={
                    "StringEquals": {
                        "cognito-identity.amazonaws.com:aud": identity_pool.ref
                    },
                    "ForAnyValue:StringLike": {
                        "cognito-identity.amazonaws.com:amr": "unauthenticated"
                    }
                },
                assume_role_action="sts:AssumeRoleWithWebIdentity"
            )
        )

        default_policy = cognito.CfnIdentityPoolRoleAttachment(self, "DefaultValid",
            identity_pool_id=identity_pool.ref,
            roles={
                "authenticated": authenticated_role.role_arn,
                "unauthenticated": unauthenticated_role.role_arn
            }
        )

        core.CfnOutput(self, "KheloUserPoolId", value=user_pool.user_pool_id)
        core.CfnOutput(self, "KheloUserPoolClientId", value=user_pool_client.user_pool_client_id)
        core.CfnOutput(self, "KheloIdentityPoolId", value=identity_pool.ref)
        core.CfnOutput(self, "KheloAuthenticatedRoleArn", value=authenticated_role.role_arn)
        core.CfnOutput(self, "KheloAuthenticatedRoleName", value=authenticated_role.role_name)
        core.CfnOutput(self, "KheloUnauthenticatedRoleArn", value=unauthenticated_role.role_arn)
        core.CfnOutput(self, "KheloUnauthenticatedRoleName", value=unauthenticated_role.role_name)
