from aws_cdk import (
    core,
    aws_codecommit as codecommit,
    aws_amplify as amplify
)
from functools import partial

class AmplifyBackendStack(core.Stack):

    def __init__(self, app: core.App, id: str, props, **kwargs) -> None:
        super().__init__(app, id, **kwargs)

        khelo_web_codecommit_repo = codecommit.Repository(self, "KheloWebRepository",
            repository_name="khelo-web",
            description="Khelo client for the Web"
        )

        khelo_amplify_app = amplify.App(self, "KheloWebApp",
            source_code_provider=amplify.CodeCommitSourceCodeProvider(repository=khelo_web_codecommit_repo)
        )

        mainBranch = khelo_amplify_app.add_branch("KheloMainBranch", branch_name="main")
        mainBranch.add_environment("STAGE", "dev")

        core.CfnOutput(self, "KheloWebCodeCommitRepoName", value=khelo_web_codecommit_repo.repository_name)
        core.CfnOutput(self, "KheloWebCodeCommitRepoArn", value=khelo_web_codecommit_repo.repository_arn)
        core.CfnOutput(self, "KheloWebCodeCommitRepoCloneHttp", value=khelo_web_codecommit_repo.repository_clone_url_http)
        core.CfnOutput(self, "KheloWebCodeCommitRepoCloneSsh", value=khelo_web_codecommit_repo.repository_clone_url_ssh)
        core.CfnOutput(self, "KheloAmplifyAppId", value=khelo_amplify_app.app_id)
        core.CfnOutput(self, "KheloAmplifyAppName", value=khelo_amplify_app.app_name)
        core.CfnOutput(self, "KheloAmplifyAppArn", value=khelo_amplify_app.arn)
        core.CfnOutput(self, "KheloAmplifyAppDefaultDomain", value=khelo_amplify_app.default_domain)
        core.CfnOutput(self, "KheloAmplifyAppMainBranchArn", value=mainBranch.arn)
