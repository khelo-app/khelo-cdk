
# Khelo CDK

## How do I get set up with testing local development? ##

Below instructions assumes the local dev environment is a linux distribution. 

For local development and testing we need to utilize the [aws-codebuild-agent](https://docs.aws.amazon.com/codebuild/latest/userguide/use-codebuild-agent.html) script. For more hands-on details look into [this](https://www.jakelitwicki.com/2019/03/14/aws-codebuild-locally/), [this](https://medium.com/@jagannathsrs/testing-and-debugging-aws-codebuild-buildspecs-locally-9166d3686de0) or [this](https://dev.to/aws-builders/run-local-graviton2-builds-with-aws-codebuild-agent-1o6h).

Following successful setup of the environment we can execute the below command from the project root directory -

```
.../khelo-cdk> codebuild_build.sh -i aws/codebuild/standard:5.0 -a /tmp/khelo-cdk-output
```

If the build is successful we can find the generated artifacts in the `/tmp/khelo-cdk-output` directory.

## Contribution guidelines ##

* Confirm that the local deployment is successful using the aws-codebuild-agent tool before pushing any code change for review.
