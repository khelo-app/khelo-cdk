from aws_cdk import core
from stacks.pipeline_stack import PipelineStack
from stacks.auth_stack import AuthStack
from stacks.amplify_backend_stack import AmplifyBackendStack

app = core.App()

amplify_env = core.Environment(
    account=app.node.try_get_context("amplify_aws_account"),
    region="us-east-1"
)

# PipelineStack(app, "PipelineStack", None)

AuthStack(app, "KheloAuthStack", None, env=amplify_env)

AmplifyBackendStack(app, "KheloAmplifyBackendStack", None, env=amplify_env)

app.synth()
